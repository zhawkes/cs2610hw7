from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from .models import Conversion

# Create your views here.
def index(request):
  units = request.GET['from']
  value = request.GET['value']
  to = request.GET['to']
  conversion = Conversion.objects.first();
  try:
    factor = getattr(conversion, units);
    convert = factor * float(value);
    data = {
      "units" : "t_oz",
      "value": convert
    }
  except:
    data = {
      "error": "Invalid request, try again please"
    }
  return JsonResponse(data);
def init(request):
  conversion = Conversion();
  conversion.save()
  return HttpResponseRedirect("/gold/")
