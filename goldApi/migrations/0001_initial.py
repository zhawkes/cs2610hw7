# Generated by Django 2.0.2 on 2018-04-26 05:09

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Conversion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lbs', models.FloatField(default=14.583)),
                ('oz', models.FloatField(default=0.911)),
                ('grams', models.FloatField(default=0.032)),
                ('kgs', models.FloatField(default=32.15)),
                ('tons', models.FloatField(default=29166)),
            ],
        ),
    ]
