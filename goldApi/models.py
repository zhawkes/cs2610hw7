from django.db import models

# Create your models here.
#this is for the api
class Conversion(models.Model):
  lbs = models.FloatField(default=14.583)
  oz = models.FloatField(default=0.911)
  grams = models.FloatField(default=0.032)
  kgs = models.FloatField(default=32.150)
  tons = models.FloatField(default=29166)

