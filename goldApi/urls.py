from django.conf.urls import url
from django.urls import path
from . import views

app_name='goldApi'
urlpatterns = [
  url(r'^$', views.index, name='index'),
  url('init', views.init, name="init")
]